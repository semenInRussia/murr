import pytest
from django.contrib.auth import get_user_model
from django.core import mail
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

Murren = get_user_model()


@pytest.mark.django_db
def test_create_murren(murren_data):
    username = murren_data['username']
    password = murren_data['password']
    api = APIClient(enforce_csrf_checks=True)

    assert Murren.objects.count() == 0

    url = reverse('murren-list')
    response = api.post(url, murren_data, format='json')
    resp_content = response.json()

    assert response.status_code == status.HTTP_201_CREATED
    assert Murren.objects.count() == 1
    assert len(mail.outbox) == 1

    murren = Murren.objects.get(id=resp_content['id'])
    assert murren.username == murren_data['username']
    assert murren.password is not None
    assert murren.is_active is False

    html_mail_body = mail.outbox[0].alternatives[0][0]
    assert username in html_mail_body
    raw_list = html_mail_body.split('___')
    uid, token = raw_list[1], raw_list[2]

    url = reverse('murren-activation')
    data = {
        'uid': uid,
        'token': token,
    }
    response = api.post(url, data, format='json')
    murren.refresh_from_db()
    assert response.status_code == 204
    assert murren.is_active is True

    data = {
        'username': username,
        'password': password,
    }
    url = reverse('obtain_jwt_token')
    response = api.post(url, data, format='json')

    assert response.status_code == 200
    assert 'token' in response.json()
