from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from django.contrib.auth import get_user_model
from django.core.files.images import ImageFile
from django.http import HttpResponse
from rest_auth.registration.views import SocialLoginView
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from murren.serializers import MurrenSerializer
from .permissions import IsAuthenticatedAndMurrenOrReadOnly
from .services import make_resize_image, make_crop_image

Murren = get_user_model()


class MurrenViewSet(ModelViewSet):
    queryset = Murren.objects.filter(is_active=True).order_by('-date_joined')
    serializer_class = MurrenSerializer
    permission_classes = [IsAuthenticatedAndMurrenOrReadOnly]

    @action(detail=False, permission_classes=[IsAuthenticated])
    def profile(self, request):
        murren = MurrenSerializer(instance=request.user)
        return Response(murren.data)

    @action(detail=False, permission_classes=[IsAuthenticated], methods=['POST'])
    def avatar_resize(self, request):
        file_avatar = request.FILES.get('avatar')
        b_image, error = make_resize_image(file_avatar, (500, 500))
        if error:
            return Response({"error": error}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return HttpResponse(b_image, content_type='image/jpeg')

    @action(detail=False, permission_classes=[IsAuthenticated], methods=['PATCH'])
    def avatar_update(self, request):
        file_avatar = request.FILES.get('avatar')
        crop_x = int(request.data.get('crop.x'))
        crop_y = int(request.data.get('crop.y'))
        crop_width = int(request.data.get('crop.width'))
        crop_height = int(request.data.get('crop.height'))

        b_image, error = make_crop_image(
            file_avatar, (crop_x, crop_y, crop_width, crop_height))

        if error:
            return Response({"error": error}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        murren_avatar = ImageFile(b_image, name=f"{request.user.pk}.jpeg")
        if request.user.murren_avatar != 'default_murren_avatar.png':
            request.user.murren_avatar.delete()
        request.user.murren_avatar = murren_avatar
        request.user.save()

        serializer = self.get_serializer(instance=request.user)

        return Response(serializer.data)


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter
