import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient


@pytest.mark.django_db
def test_like_murr_card(create_release_murr):
    murren = create_release_murr['murren']
    murr = create_release_murr['murr_card']
    api = APIClient()
    api.force_authenticate(user=murren)

    url = reverse('murr_card-like', kwargs={'pk': murr.id})

    response_like = api.post(url)
    murr.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == 1
    assert murr.rating == 1
    assert murr.liked_murrens.first() == murren

    response_like = api.post(url)
    murr.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == 0
    assert murr.rating == 0
    assert murr.liked_murrens.first() is None


@pytest.mark.django_db
def test_like_murr_card_not_authenticated(create_release_murr):
    murr = create_release_murr['murr_card']
    api = APIClient()

    url = reverse('murr_card-like', kwargs={'pk': murr.id})
    response = api.post(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_dislike_murr_card(create_release_murr):
    murren = create_release_murr['murren']
    murr = create_release_murr['murr_card']
    api = APIClient()
    api.force_authenticate(user=murren)

    url = reverse('murr_card-dislike', kwargs={'pk': murr.id})

    response_like = api.post(url)
    murr.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == -1
    assert murr.rating == -1
    assert murr.disliked_murrens.first() == murren

    response_like = api.post(url)
    murr.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == 0
    assert murr.rating == 0
    assert murr.disliked_murrens.first() is None


@pytest.mark.django_db
def test_dislike_murr_card_not_authenticated(create_release_murr):
    murr = create_release_murr['murr_card']
    api = APIClient()

    url = reverse('murr_card-dislike', kwargs={'pk': murr.id})
    response = api.post(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_like_murr_comment(create_murr_comment):
    murren = create_murr_comment['murren']
    murr_comment = create_murr_comment['murr_comment']
    api = APIClient()
    api.force_authenticate(user=murren)

    url = reverse('comment-like', kwargs={'pk': murr_comment.id})

    response_like = api.post(url)
    murr_comment.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == 1
    assert murr_comment.rating == 1
    assert murr_comment.liked_murrens.first() == murren

    response_like = api.post(url)
    murr_comment.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == 0
    assert murr_comment.rating == 0
    assert murr_comment.liked_murrens.first() is None


@pytest.mark.django_db
def test_like_murr_comment_not_authenticated(create_murr_comment):
    murr_comment = create_murr_comment['murr_comment']
    api = APIClient()

    url = reverse('comment-like', kwargs={'pk': murr_comment.id})
    response = api.post(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_dislike_murr_comment(create_murr_comment):
    murren = create_murr_comment['murren']
    murr_comment = create_murr_comment['murr_comment']
    api = APIClient()
    api.force_authenticate(user=murren)

    url = reverse('comment-dislike', kwargs={'pk': murr_comment.id})

    response_like = api.post(url)
    murr_comment.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == -1
    assert murr_comment.rating == -1
    assert murr_comment.disliked_murrens.first() == murren

    response_like = api.post(url)
    murr_comment.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == 0
    assert murr_comment.rating == 0
    assert murr_comment.disliked_murrens.first() is None


@pytest.mark.django_db
def test_dislike_murr_comment_not_authenticated(create_murr_comment):
    murr_comment = create_murr_comment['murr_comment']
    api = APIClient()

    url = reverse('comment-like', kwargs={'pk': murr_comment.id})
    response = api.post(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
